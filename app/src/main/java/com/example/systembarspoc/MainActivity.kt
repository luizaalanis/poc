package com.example.systembarspoc

import android.annotation.SuppressLint
import android.app.admin.DevicePolicyManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat

class MainActivity : AppCompatActivity() {

    private val enableResult = 1
    lateinit var deviceManger: DevicePolicyManager
    lateinit var compName: ComponentName
    lateinit var btnEnable: Button
    lateinit var btnLock: Button

    // immersive mode
    private fun hideSystemBars() {
        val windowInsetsController =
            ViewCompat.getWindowInsetsController(window.decorView) ?: return

        windowInsetsController.systemBarsBehavior =
            WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE

        windowInsetsController.hide(WindowInsetsCompat.Type.systemBars())
    }

   override fun onCreate(savedInstanceState: Bundle?) {
      super.onCreate(savedInstanceState)
      setContentView(R.layout.activity_main)
      title = "KotlinApp"
      btnEnable = findViewById(R.id.btnEnable)
      btnLock = findViewById(R.id.btnLock)
      deviceManger = getSystemService(Context.DEVICE_POLICY_SERVICE) as DevicePolicyManager
      compName = ComponentName(this, DeviceAdmin::class.java)
      val active = deviceManger.isAdminActive(compName)
      if (active) {
         btnEnable.text = "Disable"
         btnLock.visibility = View.VISIBLE
      }
      else {
         btnEnable.text = "Enable"
         btnLock.visibility = View.GONE
      }
   }
   fun enablePhone(view: View) {
      val active = deviceManger.isAdminActive(compName)
      if (active) {
         deviceManger.removeActiveAdmin(compName)
         btnEnable.text = "Enable"
         btnLock.visibility = View.GONE
      }
      else {
         val intent = Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN)
         intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, compName)
         intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "You should enable the app!")
         startActivityForResult(intent, enableResult)
      }
   }

   fun lockPhone(view: View) {
      deviceManger.lockNow()
   }

    @SuppressLint("WrongConstant")
    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        Log.d("Focus debug", "show block screen")

        if (!hasFocus && btnEnable.text == "Disable") {
            deviceManger.lockNow()
        }
    }

   override fun onActivityResult(requestCode: Int, resultCode: Int, @Nullable data: Intent?) {
      super.onActivityResult(requestCode, resultCode, data)
      when (requestCode) {
         enableResult -> {
                if (resultCode == RESULT_OK) {
                   btnEnable.text = "Disable"
                   btnLock.visibility = View.VISIBLE
                }
                else {
                   Toast.makeText(
                   applicationContext, "Failed!",
                   Toast.LENGTH_SHORT
                ).show()
             }
             return
         }
      }
   }
}